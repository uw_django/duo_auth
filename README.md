# Using duo_auth

Install duo_auth:

`pip install -e git+https://git.uwaterloo.ca/uw_django/duo_auth.git`

Add to your INSTALLED_APP, set the LOGIN_URL and add the authentication backend

```python

INSTALLED_APPS [
    # ...
    'duo_auth',
    # ...
]

LOGIN_URL = 'duo_auth:duo_oidc_login'

AUTHENTICATION_BACKENDS = [
    'duo_auth.backends.DuoOIDCBackend',
    'django.contrib.auth.backends.ModelBackend',
]
```
*Note:* You should no longer use `django_auth_adfs`.  Duo SSO handles the autehntication.

Include the urls

```python

urlpatterns = [
    # ...
    url(r'^duo/', include('duo_auth.urls')),
    # ...
]
```

The following options are available for configuration

- `DUO_IKEY` (Required) Duo Integration Key
- `DUO_SKEY` (Required) Duo Secret Key
- `AUTH_SERVER` (Required) Your Duo API server host eg: `https://sso-a4f68ca3.sso.duosecurity.com/oidc/XXXXXXXXXXXXXXXXX`
- `AUTH_CLAIM_MAPPING` (Optional) A mapping of claims to be used for the user.
- `AUTH_SCOPE` (Optional) A list of scopes to be used for the user. [Default: `openid profile email`]
- `USERNAME_CLAIM` (Optional) The claim to use for the username. [Default: `user_id`]
- `GROUP_CLAIM` (Optional) The claim to use for the group. [Default: null]
- `AUTO_CREATE_USER` (Optional) Automatically create users if they do not exist. [Default: `False`]


# OLD CONFIGURATION
The following options are available for configuration

- `DUO_IKEY` (Required) Duo Integration Key
- `DUO_SKEY` (Required) Duo Secret Key
- `DUO_AKEY` (Requeired) A locally [generated application key](https://duo.com/docs/duoweb#1.-generate-an-akey)
- `DUO_HOST` (Required) Your Duo API server host
- `DUO_ADMIN_PREFIX` (Optional) Base URL for admin to protect with DuoAdminMiddleware, will determine automatically if using django admin if not specified.
- `DUO_SESSION_KEY` (Optional) The session key to use for storing 2fa success [Default: 'duo_authenticated]
- `DUO_EXPIRY` (Optional) The time (in minutes) to keep duo auth active from last check [Default: 5 minutes]

---

**Using duo_auth**.

**Decorator** to require all access to a view to go through duo auth.

```python
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from duo_auth.utils import duo_required

@login_required
@duo_required
def restricted_view(request, template='index.html'):
    return render(request, template)

```

**Manually**

```python
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from duo_auth.utils import is_duo_authenticated, duo_redirect

def potentially_restricted(request, template='index.html'):
    # perform check to see if restricted
    if request.GET.get('restricted', False):
        if not is_duo_authenticated(request):
            return duo_redirect(request)

    return render(request, template)
```
---

**Recommended**: Add display that user is "in 2-factor mode"

In your base template, add this where appropriate:

```
{% load duoauth %}

...

{% two_factor_display %}

```

This will refresh the duo auth expiry on each page load and provide a link to "drop access".

The template can be override by creating your own at the following path `duo_auth/2fa_notice.html`

---

**Optional**: To enforce 2-factor in your application's admin

Add `duo_auth.middleware.DuoAdminMiddleware` to your `MIDDLEWARE` configuration after the django Authentication Middleware

```python

MIDDLEWARE = [
    # ...
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'duo_auth.middleware.DuoAdminMiddleware',
    # ...
]
```


