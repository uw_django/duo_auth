from django.conf import settings

__all__ = []

_DEFAULTS = {
    'DUO_ADMIN_PREFIX': None,
    'DUO_SESSION_KEY': 'duo_authenticated',
    'DUO_EXPIRY': 5, # Expire 2-factor after 5 minutes
    'AUTH_SCOPE': 'openid profile email',
    'AUTO_CREATE_USER': False,
    'USERNAME_CLAIM': 'user_id',
    'GROUP_CLAIM': None,
}

_REQUIRED = []

_NOT_REQUIRED = []

if 'duo_auth.backends.DuoOIDCBackend' in settings.AUTHENTICATION_BACKENDS:
    _REQUIRED = ['DUO_IKEY', 'DUO_SKEY', 'AUTH_SERVER']
    _NOT_REQUIRED = ['DUO_AKEY', 'DUO_HOST', 'DUO_LOGIN_URL', 'DUO_EXPIRY']
    if 'django_auth_adfs' in settings.INSTALLED_APPS:
        print("django_auth_adfs is in INSTALLED_APPS, but is not required when using DuoOIDCBackend")
else:
    _REQUIRED = ['DUO_IKEY', 'DUO_AKEY', 'DUO_SKEY', 'DUO_HOST']

for key, value in _DEFAULTS.items():
    try:
        getattr(settings, key)
    except AttributeError:
        setattr(settings, key, value)
    # Suppress errors from DJANGO_SETTINGS_MODULE not being set
    except ImportError:
        pass

for key in _REQUIRED:
    try:
        getattr(settings, key)
    except AttributeError:
        raise Exception('Setting "{}" must be specified to use duo_auth application'.format(key))

for key in _NOT_REQUIRED:
    if hasattr(settings, key):
        if settings.DEBUG:
            print(f"Setting '{key}' is not required when using DuoOIDCBackend, but is present")


# If the DuoOIDCBackend is in the AUTHENTICATION_BACKENDS, then we need to
# set AUTHLIB_OAUTH_CLIENTS to include the Duo OIDC client
# if not already present
if "duo_auth.backends.DuoOIDCBackend" in settings.AUTHENTICATION_BACKENDS:
    authlib_clients = getattr(settings, 'AUTHLIB_OAUTH_CLIENTS', {})
    if 'duo' not in authlib_clients:
        authlib_clients['duo'] = {
            'client_id': settings.DUO_IKEY,
            'client_secret': settings.DUO_SKEY,
        }

        setattr(settings, 'AUTHLIB_OAUTH_CLIENTS', authlib_clients)
