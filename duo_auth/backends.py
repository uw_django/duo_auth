from duo_auth.utils import get_oauth_client
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import get_user_model
from django.conf import settings
from django.utils import timezone
from datetime import timedelta


class DuoOIDCBackend(ModelBackend):
    def handle_groups(self, user, groups):
        pass

    def authenticate(self, request):
        oauth = get_oauth_client()
        proxies = getattr(settings, 'AUTH_PROXIES', None)
        token = oauth.duo.authorize_access_token(request, proxies=proxies)
        usermodel = get_user_model()

        username_claim = settings.USERNAME_CLAIM
        claim_mapping = getattr(settings, 'AUTH_CLAIM_MAPPING', {})

        group_claim = getattr(settings, 'GROUP_CLAIM', None)

        request.token = token

        request.session.set_expiry(timezone.now() + timedelta(seconds=token['expires_in']))

        update_fn = None

        if settings.AUTO_CREATE_USER:
            update_fn = usermodel.objects.update_or_create
        else:
            def get_update_user(*, username, defaults):
                try:
                    user = usermodel.objects.get(username=username)
                    user.__dict__.update(defaults)
                    user.save()
                    return user, False
                except usermodel.DoesNotExist:
                    return None, False

            update_fn = get_update_user

        update_fields = {}
        for key, value in claim_mapping.items():
            if value in token['userinfo']:
                update_fields[key] = token['userinfo'][value]

        user, created = update_fn(username=token['userinfo'][username_claim], defaults=update_fields)

        if user is not None and group_claim is not None and group_claim in token['userinfo']:
            self.handle_groups(user, token['userinfo'][group_claim])

        return user
