""" Duo Admin Middleware """

from urllib.parse import urlencode

from django.core.exceptions import PermissionDenied
from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.shortcuts import redirect
from django.http import HttpResponseNotFound, Http404
from django.utils.deprecation import MiddlewareMixin

from duo_auth.utils import is_duo_authenticated, duo_redirect

__all__ = ['DuoAdminMiddleware']

class DuoAdminMiddleware(MiddlewareMixin):
    """Middleware that forces duo two factor on admin pages"""

    def process_request(self, request):
        """ Checks that authentication middleware is installed """

        error = ("The Duo Admin middleware requires authentication "
                 "middleware to be installed. Edit your MIDDLEWARE_CLASSES "
                 "setting to insert 'django.contrib.auth.middleware."
                 "AuthenticationMiddleware'.")
        assert hasattr(request, 'user'), error

    def process_view(self, request, view_func, view_args, view_kwargs):
        """ Forwards authenticated users to duo login page """

        if settings.DUO_ADMIN_PREFIX:
            if not request.path.startswith(settings.DUO_ADMIN_PREFIX):
                return None
        elif not view_func.__module__.startswith('django.contrib.admin.'):
            return None

        authenticated = request.user.is_authenticated

        if callable(authenticated):
            authenticated = authenticated()

        if authenticated and (getattr(settings, 'DUO_ALL_USERS', True) or request.user.is_staff):
            if not is_duo_authenticated(request):
                """ User is authenticated and staff, but has not done two factor """
                return duo_redirect(request)
            else:
                """ User is authenticated and staff, and has performed two factor """
                return None
        else:
            raise Http404
