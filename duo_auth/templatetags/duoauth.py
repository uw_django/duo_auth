from django import template
from duo_auth.utils import is_duo_authenticated, get_expiry

register = template.Library()

@register.inclusion_tag('duo_auth/2fa_notice.html', takes_context=True)
def two_factor_display(context):
    request = context['request']
    is_2fa = is_duo_authenticated(request)
    return {'is_2fa': is_2fa, 'expiry': get_expiry(request), 'request': request}
