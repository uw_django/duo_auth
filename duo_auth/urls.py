"""groupman URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

try:
    from django.conf.urls import url, include
except ImportError:
    from django.urls import re_path as url, include


from duo_auth import views

app_name = 'duo_auth'

urlpatterns = [
    url(r'^login/$', views.duo_login, name="duo_login"),
    url(r'^deauth/$', views.drop_duo_auth, name="duo_drop_auth"),
    url(r'^oidc/login/$', views.duo_oidc_login, name="duo_oidc_login"),
    url(r'^oidc/callback/$', views.duo_oidc_callback, name="duo_oidc_callback"),
]
