from django.conf import settings
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.sites.shortcuts import get_current_site
from authlib.integrations.django_client import OAuth

OAUTH = None

try:
    from django.utils.http import is_safe_url
except ImportError:
    from django.utils.http import url_has_allowed_host_and_scheme as is_safe_url

try:
    from django.utils.http import urlquote
except ImportError:
    from urllib.parse import quote as urlquote

from django.shortcuts import render, resolve_url, redirect
from django.utils import timezone as django_tz

from datetime import timedelta, timezone, datetime

from functools import wraps

import urllib

def duo_username(user):
    return user.username

def is_duo_authenticated(request, refresh=True):
    expiry_key = settings.DUO_SESSION_KEY + '_expires'
    valid = False
    authenticated = request.user.is_authenticated
    if callable(authenticated):
        authenticated = authenticated()
    if authenticated:
        expiry = get_expiry(request)
        if expiry is not None and expiry > django_tz.now():
            if request.session.get(settings.DUO_SESSION_KEY, None) == duo_username(request.user):
                if refresh:
                    set_expiry(request, django_tz.now() + timedelta(minutes=settings.DUO_EXPIRY))
                valid = True
    return valid

def get_expiry(request):
    expiry_key = settings.DUO_SESSION_KEY + '_expires'
    return datetime.fromtimestamp(request.session.get(expiry_key, 0), tz=timezone.utc)

def set_expiry(request, date):
    expiry_key = settings.DUO_SESSION_KEY + '_expires'
    request.session[expiry_key] = date.timestamp()

def duo_authenticate(request):
    expiry_key = settings.DUO_SESSION_KEY + '_expires'
    request.session[settings.DUO_SESSION_KEY] = duo_username(request.user)
    set_expiry(request, django_tz.now() + timedelta(minutes=settings.DUO_EXPIRY))

def duo_unauthenticate(request):
    try:
        del request.session[settings.DUO_SESSION_KEY + '_expires']
        del request.session[settings.DUO_SESSION_KEY]
    except KeyError:
        pass

def duo_redirect(request, params={}):
    redirect_to = request.get_full_path()
    params[REDIRECT_FIELD_NAME] = redirect_to
    return redirect('{}?{}'.format(settings.DUO_LOGIN_URL, urllib.parse.urlencode(params)))

def get_redirect_url(request):
    redirect_to = request.POST.get(
        REDIRECT_FIELD_NAME,
        request.GET.get(REDIRECT_FIELD_NAME, '')
    )
    url_is_safe = is_safe_url(
        url=redirect_to,
        allowed_hosts={request.get_host()},
        require_https=request.is_secure(),
    )
    
    if not url_is_safe:
        redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

    return redirect_to

def duo_required(view_func, redirect_field_name=REDIRECT_FIELD_NAME):
    """
    Decorator for views that checks that the user has been authenticated with
    Duo, redirecting to the Duo authentication page if necessary.
    """
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            if is_duo_authenticated(request):
                return view_func(request, *args, **kwargs)
            return duo_redirect(request)
        return _wrapped_view
    return decorator(view_func)

def get_oauth_client():
    global OAUTH
    if OAUTH is None:
        OAUTH = OAuth()
        OAUTH.register(
            name='duo',
            server_metadata_url=settings.AUTH_SERVER + '/.well-known/openid-configuration',
            client_kwargs={
                'scope': 'openid email profile',
                'proxies': getattr(settings, 'AUTH_PROXIES', None)
            },
        )
    return OAUTH
