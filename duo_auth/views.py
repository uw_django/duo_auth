from django.conf import settings
from django.shortcuts import render, redirect
from django.contrib.auth import REDIRECT_FIELD_NAME, authenticate, login
from django.urls import reverse

import duo_web

from duo_auth.utils import duo_username, get_redirect_url, duo_authenticate, duo_unauthenticate, duo_redirect, get_oauth_client

def duo_login(request, template="duo_auth/duo_login.html"):
    if request.method == 'GET':
        message = request.GET.get(
            'message', 'Secondary authorization required.')
        next_page=request.GET.get(REDIRECT_FIELD_NAME, settings.LOGIN_REDIRECT_URL)
        sig_request = duo_web.sign_request(
            settings.DUO_IKEY, settings.DUO_SKEY, settings.DUO_AKEY,
            duo_username(request.user))
        return render(request, template, dict(
            message=message,
            next=next_page,
            duo_host=settings.DUO_HOST,
            post_action=request.path,
            sig_request=sig_request,
        ))
    elif request.method == 'POST':
        sig_response = request.POST.get('sig_response', '')
        duo_user = duo_web.verify_response(
            settings.DUO_IKEY, settings.DUO_SKEY, settings.DUO_AKEY,
            sig_response)
        next_page = get_redirect_url(request)
        if duo_user is None:
            # Redirect user to try again, keeping the next argument.
            # Note that we don't keep any other arguments.
            arg_map = {'message': 'Duo access denied.'}
            if next_page:
                arg_map['next'] = next_page
            return duo_redirect(request, arg_map)
        else:
            next_page = get_redirect_url(request)
            duo_authenticate(request)
            return redirect(next_page)

def drop_duo_auth(request):
    next_page = get_redirect_url(request)
    duo_unauthenticate(request)
    return redirect(next_page)


def duo_oidc_login(request, template="duo_auth/duo_login.html"):
    oauth = get_oauth_client()
    # get the next url from the request
    next_page = get_redirect_url(request)
    # save the next url in the session
    request.session['next'] = next_page

    redirect_uri = request.build_absolute_uri(reverse('duo_auth:duo_oidc_callback'))
    return oauth.duo.authorize_redirect(request, redirect_uri)


def duo_oidc_callback(request):
    user = authenticate(request)
    if user.is_active:
        login(request, user)

    # get the next url from the session or use the default
    next_page = request.session.get('next', settings.LOGIN_REDIRECT_URL)

    return redirect(next_page)
