try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(
    name='duo_auth',
    version='2.0',
    description='Django Duo Integration',
    author='Ryan Goggin',
    author_email='ryan.goggin@uwaterloo.ca',
    url='https://git.uwaterloo.ca/uw_django/duo_auth',
    packages=['duo_auth'],
    license='Apache',
    classifiers=[
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: Apache License',
    ],
    include_package_data=True,
    install_requires=[
        'duo_web',
        'Authlib==1.2.1',
    ]
)
